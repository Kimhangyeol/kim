#!/usr/bin/env perl -w
use strict;
use Data::Dumper;
use Data::Printer;
use List::Util qw/ max min sum /;

#용돈 기입장

open my $out, ">>", "pin_money_note.txt" or die;
my @pm = ();
my ( $num, $year, $month, $day ) = "";
my $money = 0;

sub main {
    print "1: add year\t2: add month\t3: add day\t4: exit\nchoose: ";
    $num = <STDIN>;
    if ( $num == 1 ) {
        year();
    }
    elsif ( $num == 2 ) {
        month();
    }
    elsif ( $num == 3 ) {
        day();
    }
    elsif ( $num == 4 ) {
        exit;
    }
}

sub year {
    print "Input year: ";
    $num = <STDIN>;
    $year = $num;
    chomp ( $year );

    print $out "\n                          $year년\n";
    main();
}

sub month {
    print "Input month: ";
    $num = <STDIN>;
    $month = $num;
    chomp ( $month );

    print $out "\n$month월\n";
    print $out "----------------------------------------------------------------------\n";
    main();
}

sub day {
    print "Input day: ";
    $num = <STDIN>;
    $day = $num;
    chomp ( $day );

    print $out "\t$day일  |  ";
    print "1: Content Input 2: Add day 3: main 3: exit\n";
    print "choose: ";
    $num = <STDIN>;
    if ( $num == 1 ) {
        content();
    }
    elsif ( $num == 2 ) {
        day();
    }
    elsif ( $num == 3 ) {
        main();
    }
    elsif ( $num == 4 ) {
        exit;
    }
}

sub content {
    my @arr = ();
    my ( $in_money, $out_money ) = "";

    print "Please Input your contents\n";
    print "\nYour money: $money\n\n";
    print "1: in 2: out\nchoose: ";
    chomp ( $num = <STDIN> );
    $money == 0 ? print $out "  0  " : print $out "$money";
    if ( $num == 1 ) {
        print $out "  |  수입 ";
        print "How much: ";
        chomp ( $num = <STDIN> );
        $in_money = $num;
        $money += $in_money;
        print $out "$in_money  |  ";
    }
    elsif ( $num == 2 ) {
        print $out "  |  지출 ";
        print "How much: ";
        chomp ( $num = <STDIN> );
        $out_money = $num;
        $money -= $out_money;
        print $out "$out_money  |  ";
    }

    print "Input Contents: ";
    chomp ( $num = <STDIN> );
    print $out "$num  |  잔액 $money  |";
    print $out "\n";

    print "Success!\n1: Add day 2: Main 3: exit\nchoose: ";
    chomp ( $num = <STDIN> );
    if ( $num == 1 ) {
        day();
    }
    elsif ( $num == 2 ) {
        main();
    }
    elsif ( $num == 3 ) {
        exit;
    }
}

sub money {
    while ( my $line = <> ) {
        chomp $line;
        if ( $line =~ /.*잔액.(\d+).*/ ) {
            $money = $1;
        }
    }
}





money();
main();
close $out;
